﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace ChatBot
{
    public partial class Form1 : Form
    {

        bool active = false;
        private BackgroundWorker bw = new BackgroundWorker();

        int interval = 1000; //default at 1020


        public Form1()
        {
            InitializeComponent();

            bw.WorkerReportsProgress = true;
            bw.WorkerSupportsCancellation = true;
            bw.DoWork += new DoWorkEventHandler(bw_Wow);
            bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bw_WowComplete);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!bw.IsBusy)
            {
                active = true;
                bw.RunWorkerAsync();
            }

        }//close button1

        private void button2_Click(object sender, EventArgs e)
        {
            if (bw.IsBusy)
            {
                active = false;
                bw.CancelAsync();
            }
        }//close bw_WowComplete

        private void bw_Wow(object sender, EventArgs e)
        {
            BackgroundWorker worker = (BackgroundWorker) sender;

            while (!worker.CancellationPending)
            {
                SendKeys.SendWait("3");
                System.Threading.Thread.Sleep(interval);
                progressChanged();
            }//close while loop


        }//close bw_Wow


        private void progressChanged()
        {
            int temp = Convert.ToInt32(label3.Text) + 1;
            if (temp >= 50 && temp <=125)
            {
                interval = 1100;
            }
            else if (temp > 125)
            {
                interval = 1300;
            }
        }//close ProgressChanged

        private void bw_WowComplete(object sender, EventArgs e)
        {
            label3.Text = "0";
        }






    }
}
